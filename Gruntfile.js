module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: { // task for linting js files
            files: ['Gruntfile.js', 'src/js/**/*.js']
        },
        clean: ['dist'],
        concat: { // task for concatenating files
            dist: {
                files: [
                    {
                        src: ['src/css/**/*.css', 'src/css/custom.css'],
                        dest: 'dist/css/style.css'
                    },
                    {
                        src: ['src/modules/**/*.js', 'src/js/**/*.js', 'src/js/custom.js'],
                        dest: 'dist/js/script.js'
                    }
                ]
            }
        },
        copy: { // task for copying files from 'src' to 'dist'
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/',
                        src: ['**/*.html', '**/*.js', '**/*.css', 'img/**/*', '!js/custom.js', '!css/custom.css'],
                        dest: 'dist/'
                    }
                ]
            }
        },
        uglify: { // task for compressing files
            options: {
                banner: '/**\n * <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n */\n'
            },
            dist: {
                files: [
                    {
                        src: 'dist/js/script.js',
                        dest: 'dist/js/script.min.js'
                    }
                ]
            }
        },
        cssmin: { // task for compressing css files
            dist: {
                files: [
                    {
                        src: 'dist/css/style.css',
                        dest: 'dist/css/style.min.css'
                    }
                ]
            }
        },
        watch: { // task for watching for changes and running some task based on it
            src: {
                files: ['src/**/*.html', 'src/**/*.css', 'src/**/*.js'],
                tasks: ['default']
            }
        }
    });

    // Load the npm plugins
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Test task(s)
    grunt.registerTask('test', ['jshint']);

    // Default task(s).
    grunt.registerTask('default', ['clean', 'copy', 'concat', 'uglify', 'cssmin']);

};
