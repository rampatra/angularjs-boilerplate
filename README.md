# iw-analytics-fe
Frontend for analytics tool

## Getting Started

**Prerequisites**

1. Node.js:
 
    Install Node.js from nodejs.org
    
2. Node package manager (npm):

    Once you’ve installed Node.js, you can make sure you’ve got the very most recent version of npm using npm itself:
    
    Open terminal and run `sudo npm install npm -g`
    
3. grunt-cli:

    Install `grunt-cli` by running the command `sudo npm install -g grunt-cli`

Now that we have installed all the prerequisites, we can now build/run our project by performing the following steps:

**How to build**

1. `git clone https://github.com/BSBPortal/iw-analytics-fe.git` (for first time only)
2. `cd` into directory
3. run command `npm install` to install dependencies
4. run command `npm build` to build application

**How to run**

1. `cd` into directory
2. run command `npm start` (optionally run `grunt watch` to sync/compile files automatically while developing)

**Deployment**

1. Build the application.
2. Copy `dist` and `node_modules` directory to any parent directory and deploy the parent directory. 
3. Configure the server such that the root of the application is parent directory and it will open `dist/index.html` as 
the default index file.