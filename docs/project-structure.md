## Project Structure

<pre>
/root
|
|- /src
|   |
|   |- app.js
|   |
|   |- /css 
|   |
|   |- /js
|   |
|   |- /img
|   |
|   |- /modules
|        |
|        |- /any_module
|        |    |
|        |    |- module.controller.js
|        |    |- module.service.js 
|        |    |- index.html
|        |
|        |- /new_module
|
|- /node_modules
|
|- /dist
|
|- /test
</pre>

**Legends**

`/src` - All source files stay. Development is done on these files.  
`/dist` - After build (file compressions, scss to css conversion etc.) files stay here.  
`/test` - All test cases will go here.  
`/src/modules` - Contains all modules with controllers, services etc. in their respective directory.  
`*.controller.js` - Access data entered by the user on the UI and sanitize it.  
`*.service.js` - Make API calls to the backend.  
`app.js` - Main js file for Angular configuration/routes/constants etc.

## 


