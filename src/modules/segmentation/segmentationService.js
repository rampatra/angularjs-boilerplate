/**
 * Created by ramswaroop on 1/22/16.
 */
app.factory('SegmentationService', ['$http', '$cookies', 'config', function SegmentationFactory($http, $cookies, config) {
    return {
        getAllSegmentation: function () {
            return $http({
                method: 'GET',
                url: config.apiBaseUrl + '/v1/segment/all?clientId=1000'
            });
        },
        getAllEventAttributes: function() {
            return $http({
                method: 'GET',
                url: config.apiBaseUrl + '/v1/segment/data/event-group?clientId=1000'
            })
        },
        getEventAttributeValues : function(eventAttribute) {
            return $http({
                method: 'GET',
                url: config.apiBaseUrl + '/v1/segment/data/property-group?clientId=1000&attribute=' + eventAttribute
            })
        },
        getAllUserAttributes : function() {
            return $http({
                method: 'GET',
                url: config.apiBaseUrl + '/v1/segment/data/user-group?clientId=1000'
            })
        },
        getAllDeviceAttributes: function() {
            return $http({
                method: 'GET',
                url: config.apiBaseUrl + '/v1/segment/data/device-group?clientId=1000'
            })
        },
        saveSegmentation: function(data) {
            return $http({
                method: 'POST',
                url: config.apiBaseUrl + '/v1/segment/data/save',
                data: data
            })
        }
    }
}]);