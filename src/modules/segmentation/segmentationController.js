/**
 * Created by ramswaroop on 1/18/16.
 */
app.controller('SegmentationController', ['$scope', '$location', '$mdToast', 'Auth', 'User', 'SegmentationService', function ($scope, $location, $mdToast, Auth, User, SegmentationService) {

    // hard-coded values
    $scope.attributeTypes = ['EVENT', 'USER', 'DEVICE'];
    $scope.segmentRelationships = ['and', 'or'];
    $scope.attributes = function (attr) {
        return {EVENT: $scope.eventAttributes, USER: $scope.userAttributes, DEVICE: $scope.deviceAttributes}[attr];
    };

    // fetched values from APIs
    $scope.eventAttributes = [
        {
            type: "EVENT",
            property: {
                name: "INSTALL",
                value: null,
                dataType: null,
                operator: null,
                valueFrom: null,
                valueTo: null,
                subProperty: {
                    name: "APP_VERSION",
                    value: "1.0",
                    dataType: "STRING"
                }
            }
        },
        {
            type: "EVENT",
            property: {
                name: "INSTALL",
                value: null,
                dataType: null,
                operator: null,
                valueFrom: null,
                valueTo: null,
                subProperty: {
                    name: "PRODUCT_NAME",
                    value: "1.0",
                    dataType: "STRING"
                }
            }
        }
    ];
    $scope.eventAttributeValues = [
        {
            name: "APP_VERSION",
            value: "1.0",
            dataType: "STRING"
        },
        {
            name: "APP_VERSION",
            value: "1.0",
            dataType: "STRING"
        }
    ];
    $scope.userAttributes = [
        {
            type: "USER",
            property: {
                name: "AGE",
                value: null,
                dataType: null,
                operator: null,
                valueFrom: null,
                valueTo: null,
                subProperty: {
                    name: "APP_VERSION",
                    value: "1.0",
                    dataType: "STRING"
                }
            }
        },
        {
            type: "USER",
            property: {
                name: "SEX",
                value: null,
                dataType: null,
                operator: null,
                valueFrom: null,
                valueTo: null,
                subProperty: {
                    name: "APP_VERSION",
                    value: "1.0",
                    dataType: "STRING"
                }
            }
        }
    ];
    $scope.deviceAttributes = [
        {
            type: "DEVICE",
            property: {
                name: "OS",
                value: null,
                dataType: null,
                operator: null,
                valueFrom: null,
                valueTo: null,
                subProperty: null
            }
        },
        {
            type: "DEVICE",
            property: {
                name: "VERSION",
                value: null,
                dataType: null,
                operator: null,
                valueFrom: null,
                valueTo: null,
                subProperty: null
            }
        }
    ];
    $scope.allSegmentation = [];


    // default values
    $scope.selectedSegmentRelationship = $scope.segmentRelationships[0];


    $scope.segments = [
        {
            include: true,
            durationFrom: 1454122773615,
            durationTo: 1455322773615,
            attributes: [{
                type: "EVENT",
                property: {
                    name: "INSTALL",
                    value: null,
                    dataType: null,
                    operator: null,
                    valueFrom: null,
                    valueTo: null,
                    subProperty: {
                        "name": "APP_VERSION",
                        "value": "1.0",
                        "dataType": "STRING"
                    }
                }
            }]
        }
    ];

    // final segmentation object to save
    $scope.segmentation = {
        clientId: 1000,
        durationFrom: 1454122773615,
        durationTo: 1455322773615,
        segmentName: 'abc',
        segmentRelationship: $scope.selectedSegmentRelationship,
        segments: $scope.segments
    };

    $scope.getAllEventAttributes = function () {
        SegmentationService.getAllEventAttributes().then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                console.log(response.data.data);
                //$scope.eventAttributes = response.data.data;
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem fetching event attributes!'));
        });
    };

    $scope.getEventAttributeValues = function (eventAttr) {
        SegmentationService.getEventAttributeValues(eventAttr).then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                console.log(response.data.data);
                //$scope.eventAttributeValues = response.data.data;
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem fetching event values!'));
        });
    };

    $scope.getAllUserAttributes = function () {
        SegmentationService.getAllUserAttributes().then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                console.log(response.data.data);
                //$scope.userAttributes = response.data.data;
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem fetching user attributes!'));
        });
    };

    $scope.getAllDeviceAttributes = function () {
        SegmentationService.getAllDeviceAttributes().then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                console.log(response.data.data);
                //$scope.deviceAttributes = response.data.data;
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem fetching device attributes!'));
        });
    };

    $scope.getAllSegmentation = function () {
        SegmentationService.getAllSegmentation().then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                console.log(response.data.data);
                $scope.allSegmentation = response.data.data;
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem fetching all segmentation!'));
        });
    };

    $scope.saveSegmentation = function () {
        SegmentationService.saveSegmentation($scope.segmentation).then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                console.log(response.data.data);
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem saving segmentation!'));
        });
    };

    $scope.getAllEventAttributes();
    $scope.getAllUserAttributes();
    $scope.getAllDeviceAttributes();
}]);