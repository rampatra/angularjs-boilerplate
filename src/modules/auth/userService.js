/**
 * Created by ramswaroop on 1/22/16.
 */
app.factory('User', ['$http', function UserFactory() {
    var user = null;
    return {
        setUser: function(usr) {
            user = usr;    
        },
        getUser: function() {
            return user;
        },
        isLoggedIn: function() {
            return user !== null;
        }
    };
}]);