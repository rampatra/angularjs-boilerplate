/**
 * Created by ramswaroop on 1/22/16.
 */
app.factory('Auth', ['$http', '$cookies', 'config', function AuthFactory($http, $cookies, config) {
    return {
        login: function (data) {
            return $http({
                method: 'POST',
                url: config.apiBaseUrl + '/v1/user/login',
                data: data
            });
        },
        logout: function (data) {
            return $http({
                method: 'POST', url: config.apiBaseUrl + '/v1/user/logout',
                data: data
            });
        },
        signUp: function (data) {
            return $http({
                method: 'POST', url: config.apiBaseUrl + '/v1/user/register',
                data: data
            });
        },
        setCookie: function (user) {
            $cookies.putObject('User', user);
        },
        setToken: function(token) {
            $cookies.put('AUTH_TOKEN', token);
        },
        setSession: function(user, token) {
            this.setCookie(user);
            this.setToken(token);
        },
        removeSession: function() {
            $cookies.remove('User');
            $cookies.remove('AUTH_TOKEN');
        }
    }
}]);