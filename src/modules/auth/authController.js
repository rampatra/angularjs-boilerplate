/**
 * Created by ramswaroop on 1/18/16.
 */
app.controller('LoginController', ['$scope', '$location', '$cookies', '$mdToast', 'Auth', 'User', function ($scope, $location, $cookies, $mdToast, Auth, User) {
    $scope.login = function () {
        var data = {
            email: $scope.email,
            password: $scope.password
        };
        Auth.login(data).then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                User.setUser(response.data.data);
                Auth.setSession(User.getUser(), response.headers('AUTH_TOKEN'));
                $location.path('/dashboard');
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem logging in!'));
        });
    };
}]);

app.controller('SignUpController', ['$scope', '$location', 'Auth', 'User', function ($scope, $location, Auth, User) {
    $scope.signUp = function () {
        Auth.signUp($scope).success(function (data) {

        });
    }
}]);