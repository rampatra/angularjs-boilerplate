/**
 * Created by ramswaroop on 1/18/16.
 */
var app = angular.module('IgniteAnalytics', ['ngRoute', 'ngCookies', 'ngMaterial']);

app.constant('config', {
    apiBaseUrl: 'http://localhost:8080'
});

app.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'dist/modules/auth/login.html'
    }).when('/signup', {
        templateUrl: 'dist/modules/auth/signup.html'
    }).when('/dashboard', {
        templateUrl: 'dist/modules/dashboard/index.html'
    }).when('/segmentation', {
        templateUrl: 'dist/modules/segmentation/index.html'
    }).when('/campaign', {
        templateUrl: 'dist/modules/campaign/index.html'
    });
    $httpProvider.defaults.withCredentials = true;
}]);

app.controller('IndexController', ['$rootScope', '$scope', '$cookies', '$location', '$timeout', '$mdSidenav', '$mdToast', '$log', 'Auth', 'User', function ($rootScope, $scope, $cookies, $location, $timeout, $mdSidenav, $mdToast, $log, Auth, User) {
    $rootScope.User = function () {
        //return ($cookies.getObject('User') == null) ? User.getUser() : $cookies.getObject('User');
        return User.getUser();
    };
    $scope.toggleLeft = buildDelayedToggler('left');
    $scope.logout = function () {
        Auth.logout(null).then(function (response) {
            if (response.data.status != null && response.data.status == "success") {
                User.setUser(null);
                Auth.removeSession();                
                $location.path('/');
            }
        }, function (response) {
            $mdToast.show($mdToast.simple().textContent('Problem logging out!'));
        });
    };
    $scope.closeSideNav = function () {
        $mdSidenav('left').close()
            .then(function () {
                //$log.debug("close LEFT is done");
            });
    };

    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait, context) {
        var timer;
        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function () {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
        return debounce(function () {
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                    //$log.debug("toggle " + navID + " is done");
                });
        }, 100);
    }
}]);